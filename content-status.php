<!-- <h1><?php the_author(); ?></h1>

<div class="intro">
	<h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( 'Permalink to %s', the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php echo get_the_date(); ?></a></h2>
	<?php echo get_avatar(get_the_author_meta('user_email')); ?>
</div> -->

<!-- <div class="content status"> -->
	<div class="text">
		<h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( 'Permalink to %s', the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php echo get_the_date(); ?></a></h2>
		<?php echo get_avatar(get_the_author_meta('user_email')); ?>
	
		<h3><?php the_author(); ?></h3>
	
		<div><?php the_content(); ?></div>
	
	    <ul>
	    	<li>by <?php the_author_posts_link(); ?></li>
	        <li><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(sprintf('Permalink to %s', the_title_attribute('echo=0'))); ?>" rel="bookmark"><?php echo get_the_date(); ?></a></li>
	        <?php if(comments_open()): ?>
	        <li><?php comments_popup_link( '<span class="leave-reply">' . 'Leave a reply'. '</span>', '1 Reply', '% Replies' ); ?></li>
	        <?php endif; ?>
	        
	        <?php if(is_admin()): ?>
        	<li><?php edit_post_link( 'Edit', '<span class="edit-link">', '</span>' ); ?></li>
        	<?php endif; ?>
	    </ul>
	</div>
<!-- </div>
-->