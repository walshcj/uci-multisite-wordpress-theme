
<?php 
if(get_theme_mod('selected_homepage') == UCI_DEFAULT_UCI_HOMEPAGE) {
	wp_enqueue_style('uci-homepage1'); 
	wp_enqueue_style('flexslider-css');
} else {
	wp_enqueue_style('uci-contentpage1');
}
?>

<?php get_header(); ?>

<?php if(get_theme_mod('selected_homepage') == UCI_DEFAULT_UCI_HOMEPAGE): // user has chosen to use the UCI template layout ?>

<?php if(!ucirvine_has_homepage()): // user has no custom homepage selected so force the default one. ?>

<h1 class="visuallyhidden"><?php bloginfo('name'); ?></h1>

<div class="main-visual flexslider">
	<ul class="slides">
    	<li aria-hidden="true">
        	<a href="#"><img src="<?php bloginfo('template_url'); ?>/images/babyeater.jpg" alt=""></a>
        	<p class="flex-caption">Born in April, a young anteater clings to her mother's back at the Santa Ana Zoo, where UCI alumnus Kent Yamaguchi '83 and '84 is director. The baby's parents are Peter and Heesoo, Orange County's first pair of giant anteaters.</p>
        </li>
        <li aria-hidden="true">
        	<a href="#"><img src="<?php bloginfo('template_url'); ?>/images/grosshall.jpg" alt=""></a>
          	<p class="flex-caption">Sue and Bill Gross Hall Photo by Hoang Xuan Pham/ UC Irvine</p>
        </li>
        <li aria-hidden="true">
          	<a href="#"><img src="<?php bloginfo('template_url'); ?>/images/danhoangmichelle.jpg" alt=""></a>
          	<p class="flex-caption">Foreground (Left to Right): Photographers Daniel A. Anderson, Hoang Xuan Pham, Michelle Kim during the “University Students, summer morning” photoshoot  </p>
        </li>
	</ul>
</div>

<?php get_template_part('homepage1', 'buckets'); ?>

<div class="freetext">
      <h2 class="freetext-header">Header One</h2>
      <div class="linklist clearfix">
        <h3 class="linklist-header">List Header One</h3>
        <ul>
          <li>Powder chocolate cake sugar plum cupcake marshmallow danish.</li>
          <li>Tiramisu pastry fruitcake sugar plum macaroon lemon drops ice cream chocolate cake apple pie.</li>
          <li>Liquorice cheesecake gingerbread candy canes liquorice candy.</li>
          <li>Ice cream wafer sesame snaps.</li>
          <li>Ice cream cheesecake lemon drops marshmallow cookie chocolate cake.</li>
          <li>Souffle jelly-o candy candy gingerbread marzipan.</li>
          <li>Jujubes cotton candy jelly wafer cheesecake.</li>
          <li>Pie sesame snaps jujubes bear claw.</li>
        </ul>
      </div>
      <div class="linklist clearfix">
        <h3 class="linklist-header">List Header Two</h3>
        <ul>
          <li>Powder chocolate cake sugar plum cupcake marshmallow danish.</li>
          <li>Tiramisu pastry fruitcake sugar plum macaroon lemon drops ice cream chocolate cake apple pie.</li>
          <li>Liquorice cheesecake gingerbread candy canes liquorice candy.</li>
          <li>Ice cream wafer sesame snaps.</li>
          <li>Ice cream cheesecake lemon drops marshmallow cookie chocolate cake.</li>
          <li>Souffle jelly-o candy candy gingerbread marzipan.</li>
          <li>Jujubes cotton candy jelly wafer cheesecake.</li>
          <li>Pie sesame snaps jujubes bear claw.</li>
        </ul>
      </div>
    </div>
    
<?php else: ?>
    <?php ucirvine_get_homepage(get_theme_mod(UCI_THEME_SETTING_HOMEPAGE)); ?>
<?php endif; ?>
    
<?php elseif(get_theme_mod('selected_homepage') == UCI_DEFAULT_WP_HOMEPAGE): // user has chosen to use the default WP layout ?>

<h1><?php bloginfo('name'); ?></h1>
<h4><?php bloginfo('description'); ?></h4>

<div class="content list">
	<div class="text">
		<?php if(have_posts()): ?>
		
			<?php while (have_posts()): the_post(); ?>
                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			
			<?php if(has_post_thumbnail()): ?>
			<div class="main-visual">
				<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
			</div>
			<?php endif; ?>
			
			<div>
				<?php the_content(); ?>
			</div>
		
			<ul class="uci-post-menu clearfix">
				<li>by <?php the_author_posts_link(); ?></li>
				<li><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(sprintf('Permalink to %s', the_title_attribute('echo=0'))); ?>" rel="bookmark"><?php echo get_the_date(); ?> </a></li>
				<?php if(comments_open()): ?>
				<li><?php comments_popup_link( '<span class="leave-reply">' . 'Leave a reply'. '</span>', '1 Reply', '% Replies' ); ?></li>
				<?php endif; ?>
				
				<?php edit_post_link( 'Edit', '<li>', '</li>' ); ?>
			</ul>
			
			<?php endwhile; ?>
			
			<?php ucirvine_content_nav('nav-below'); ?>
		
		<?php else: ?>
		<?php get_template_part('content', 'none'); ?>
		<?php endif; ?>
	</div>
	
	<?php get_sidebar(); ?>
</div>
<?php else: ?>
<div>You must select a theme home page in the Admin area under Appearance</div>
<?php endif; ?>

<?php get_footer(); ?>