	</div> <!-- end class="content-container" -->
</div> <!-- end role="main" -->

<footer>
  <div class="content-container clearfix">
    
    <div class="">
      <ul>
        <!-- <li class="facebook">
          <a href="https://www.facebook.com/UCIrvine">Facebook</a>
        </li>
        <li class="twitter">
                <a href="https://twitter.com/ucirvine">Twitter</a>
        </li>
        <li class="youtube">
			<a href="http://www.youtube.com/ucirvine">YouTube</a>
		</li> -->
		<?php wp_nav_menu(array(
                    'theme_location' => UCI_MENU_FOOTER_1,
                    'items_wrap' => '<ul>%3$s</ul>',
                    'container' => ''
                )); ?>
      </ul>
    </div>
    <div>
          <ul>
                      <!--  <li>
                <a href="http://uci.edu/about/employment.php">Employment</a>
        </li>
                        <li>
                <a href="http://communications.uci.edu/campus-resources/preparedness.php">Emergency Preparedeness</a>
        </li>
                        <li>
                <a href="http://disability.uci.edu/">Disability Services</a>
        </li>
                        <li>
                <a href="http://chancellor.uci.edu/">Office of the Chancellor</a>
        </li>
                              <li>
                <br/><a href="http://universityofcalifornia.edu/">University of California</a>
        </li> -->
        	<?php wp_nav_menu(array(
                    'theme_location' => UCI_MENU_FOOTER_2,
                    'items_wrap' => '<ul>%3$s</ul>',
                    'container' => ''
                )); ?>
        	
        	<li><br /><a href="http://universityofcalifornia.edu/">University of California</a></li>
          </ul>
    </div>
    <div class="legal">
      <ul>
        <li><a href="http://uci.edu/feedback/">Feedback</a></li>
        <li><a href="http://uci.edu/privacy/">Privacy Policy</a></li>
        <li class="copyright"><br/><a href="http://uci.edu/copyright/">&#169; 2013 UC Regents</a></li>
      </ul>
    </div>
    <div class="address">
      <address>
	  	University of California, Irvine
		<br>
		Irvine, CA 92697
		<br>
		949-824-5011
      </address>
    </div>
  </div>
</footer>

<?php get_template_part('uci', 'footer'); ?>
