<h1><?php the_title(); ?></h1>
<nav class="nav-local bucket">
      <ul><?php dynamic_sidebar('contentpage1'); ?></ul>
</nav>

<div class="content">
      <div class="text">
        <?php the_content(); ?>
        <?php wp_link_pages( array( 'before' => '<div class="page-links">' . 'Pages:', 'after' => '</div>' ) ); ?>
        
        <?php if(is_admin()): ?>
       	<?php edit_post_link( 'Edit', '<span class="edit-link">', '</span>' ); ?>
        <?php endif; ?>
      </div>
</div>
