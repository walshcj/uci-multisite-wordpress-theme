<?php get_header(); ?>

<h1>Page not found.</h1>
<p>The requested URL was not found. If you entered the address manually, please check your spelling and try again. Alternatively, you can:</p>
<ul>
	<li>Use your browser's back button to return to the previous page</li>
	<li>Go the <a href="<?php bloginfo('url'); ?>">homepage</a></li>
</ul>

<?php get_footer(); ?>