<?php
/*
 * 
 * @package WordPress
 * @subpackage UC_Irvine
 * @since UC Irvine 0.1
 * @author Chris Walsh, UC Irvine
 */
ini_set('display_errors', 0);
error_reporting(E_ALL & ~E_NOTICE);

set_include_path(get_include_path() . PATH_SEPARATOR . get_stylesheet_directory() . '/includes');

@define('UCI_TEMPLATE_GLOBAL_STYLESHEET', 'http://uci.edu/css/global.min.css');
// for site specific styles
@define('UCI_TEMPLATE_SITE_STYLESHEET', get_bloginfo('stylesheet_directory') . '/css/site.css');

@define('UCI_TEMPLATE_LISTPAGE1_STYLESHEET', 'http://www.uci.edu/css/listpage-1.css');
@define('UCI_TEMPLATE_LISTPAGE2_STYLESHEET', 'http://www.uci.edu/css/listpage-2.css');
@define('UCI_TEMPLATE_CONTENTPAGE1_STYLESHEET', 'http://www.uci.edu/css/contentpage-1.css');
@define('UCI_TEMPLATE_CONTENTPAGE2_STYLESHEET', 'http://www.uci.edu/css/contentpage-2.css');
@define('UCI_TEMPLATE_CONTENTPAGE3_STYLESHEET', 'http://www.uci.edu/css/contentpage-3.css');
@define('UCI_TEMPLATE_CONTENTPAGE4_STYLESHEET', 'http://www.uci.edu/css/contentpage-4.css');
@define('UCI_TEMPLATE_RESOURCEPAGE1_STYLESHEET', 'http://www.uci.edu/css/resourcepage-1.css');

@define('UCI_TEMPLATE_FLEXSLIDER_STYLESHEET', get_bloginfo('stylesheet_directory') . '/css/flexslider-uci.min.css');

@define('UCI_TEMPLATE_HOMEPAGE1_STYLESHEET', 'http://www.uci.edu/css/homepage-1.css');

@define('UCI_TEMPLATE_ANIMATE_COLORS_JS', get_bloginfo('stylesheet_directory') . '/js/libs/jquery.animate-colors-min.js');
@define('UCI_TEMPLATE_MENU_TOGGLE_JS', get_bloginfo('stylesheet_directory') . '/js/menuToggle.min.js');
@define('UCI_TEMPLATE_VIEW_CONTROLS_JS', get_bloginfo('stylesheet_directory') . '/js/viewControls.min.js');
@define('UCI_TEMPLATE_BUCKETS_JS', get_bloginfo('stylesheet_directory') . '/js/bucketSizer.min.js');
@define('UCI_TEMPLATE_MODERNIZR_JS', get_bloginfo('stylesheet_directory') . '/js/libs/modernizr-2.5.3.min.js');

@define('UCI_TEMPLATE_FLEXSLIDER_INIT_JS', get_bloginfo('stylesheet_directory') . '/js/flexslider-init.js');
@define('UCI_TEMPLATE_FLEXSLIDER_JQUERY_JS', get_bloginfo('stylesheet_directory') . '/js/jquery.flexslider-min.js');

@define('UCI_DEFAULT_UCI_HOMEPAGE', 'uci');
@define('UCI_DEFAULT_WP_HOMEPAGE', 'wordpress');
@define('UCI_DEFAULT_SEARCH_GSA', 'gsa');
@define('UCI_DEFAULT_SEARCH_WP', 'wp');

@define('UCI_THEME_SETTING_WORDMARK', 'ucirvine_wordmark');
@define('UCI_THEME_SETTING_SEARCH', 'ucirvine_search');

@define('UCI_MENU_AUDIENCE', 'audience');
@define('UCI_MENU_MAINNAV', 'mainnav');
@define('UCI_MENU_FOOTER_1', 'footer_1');
@define('UCI_MENU_FOOTER_2', 'footer_2');

//require_once 'functions_uci_homepage.php';
require_once 'plugins/slideshow/slideshow.php';
require_once 'Customize_Wordmark_Control.php';

// register UCI template stylesheets
wp_register_style('uci-global', UCI_TEMPLATE_GLOBAL_STYLESHEET);
wp_register_style('uci-site', UCI_TEMPLATE_SITE_STYLESHEET, array('uci-contentpage1'));
wp_register_style('uci-contentpage1', UCI_TEMPLATE_CONTENTPAGE1_STYLESHEET);
wp_register_style('uci-contentpage2', UCI_TEMPLATE_CONTENTPAGE2_STYLESHEET);
wp_register_style('uci-contentpage3', UCI_TEMPLATE_CONTENTPAGE3_STYLESHEET);
wp_register_style('uci-contentpage4', UCI_TEMPLATE_CONTENTPAGE4_STYLESHEET);
wp_register_style('uci-resourcepage1', UCI_TEMPLATE_RESOURCEPAGE1_STYLESHEET);
wp_register_style('uci-listpage1', UCI_TEMPLATE_LISTPAGE1_STYLESHEET);
wp_register_style('uci-listpage2', UCI_TEMPLATE_LISTPAGE2_STYLESHEET);
wp_register_style('flexslider-css', UCI_TEMPLATE_FLEXSLIDER_STYLESHEET);
wp_register_style('uci-homepage1', UCI_TEMPLATE_HOMEPAGE1_STYLESHEET);

// register UCI template javascript
wp_register_script('jquery-1.8.3', get_bloginfo('stylesheet_directory') . '/js/jquery-1.8.3.min.js');
wp_register_script('uci-customize', get_bloginfo('stylesheet_directory') . '/js/theme_customize.js', array('jquery', 'customize-preview'), '', true);
wp_register_script('animate-colors', UCI_TEMPLATE_ANIMATE_COLORS_JS, array('jquery-1.8.3'));
wp_register_script('menu-toggle', UCI_TEMPLATE_MENU_TOGGLE_JS, array('jquery-1.8.3', 'animate-colors'));
wp_register_script('view-controls', UCI_TEMPLATE_VIEW_CONTROLS_JS, array('jquery-1.8.3'));
wp_register_script('bucket-sizer', UCI_TEMPLATE_BUCKETS_JS, array('jquery-1.8.3'));
wp_register_script('modernizr', UCI_TEMPLATE_MODERNIZR_JS);
wp_register_script('flexslider-jquery', UCI_TEMPLATE_FLEXSLIDER_JQUERY_JS, array('jquery-1.8.3'));
wp_register_script('flexslider-init', UCI_TEMPLATE_FLEXSLIDER_INIT_JS, array('flexslider-jquery'));

// Adds RSS feed links to <head> for posts and comments.
add_theme_support( 'automatic-feed-links' );

// This theme supports a variety of post formats.
add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );

// This theme uses a custom image size for featured images, displayed on "standard" posts.
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 624, 9999 ); // Unlimited height, soft crop

if(!is_admin()) {
	wp_enqueue_style('uci-global');
	wp_enqueue_style('uci-site');
	wp_enqueue_style('thickbox');
	
	wp_enqueue_script('modernizr');
	wp_enqueue_script('animate-colors');
	wp_enqueue_script('menu-toggle');
	wp_enqueue_script('view-controls');
	wp_enqueue_script('bucket-sizer');
	wp_enqueue_script('flexslider-init');
	wp_enqueue_script('thickbox');
}

function ucirvine_init() {
    register_nav_menu(UCI_MENU_AUDIENCE, 'Audience Navigation');
    register_nav_menu(UCI_MENU_MAINNAV, 'Main Navigation');
    register_nav_menu(UCI_MENU_FOOTER_1, '1st Column Footer');
    register_nav_menu(UCI_MENU_FOOTER_2, '2nd Column Footer');
}
add_action('init', 'ucirvine_init');

function ucirvine_widget_init() {
    unregister_widget('WP_Widget_Search');
}
add_action('widgets_init', 'ucirvine_widget_init');

function ucirvine_customizer_live_preview() {
	wp_localize_script('uci-customize', 'uciajax', array('ajaxurl' => admin_url('admin-ajax.php'), 'wpnonce' => wp_create_nonce('uci-nonce')));
	wp_enqueue_script('uci-customize');
}
add_action('customize_preview_init', 'ucirvine_customizer_live_preview', 11);

function ucirvine_add_thickbox($content) {
	$content = preg_replace('/<a(.*?)href="(.*?).(jpg|jpeg|png|gif|bmp|ico)"(.*?)><img/U', '<a$1href="$2.$3" $4 class="thickbox"><img', $content);
	
	return $content;
}
add_filter('the_content', 'ucirvine_add_thickbox');

/**
 * Initiate widgets for template sidebar
 */
function ucirvine_widgets_init() {
	/**
	 * main sidebar on the right, usually the default right menu for WP
	 */
	register_sidebar(array(
		'name' => 'Main Sidebar',
		'id' => 'sidebar-1',
		'description' => 'Appears on posts and pages except the optional Front Page template, which has its own widgets',
		'before_widget' => ''/*'<aside id="%1$s" class="widget %2$s">'*/,
		'after_widget' => /*'</aside>'*/'',
		'before_title' => /*'<h3 class="widget-title">'*/ '<h2 class="bucket-header">',
		'after_title' => '</h2>',
	));
	
	/**
	 * second column in footer
	 */
	register_sidebar(array(
		'name' => 'Footer Section One',
		'id' => 'footer_section_2',
		'description' => 'Use this container to populate the 1st column in the footer with content',
		'before_widget' => '<li>',
		'after_widget' => '</li>',
		'before_title' => '<span style="display:none;">',
		'after_title' => '</span>'
	));
	
	register_sidebar(array(
		'name' => 'Footer Section Two',
		'id' => 'footer_section_3',
		'description' => 'Use this container to populate the 2nd column in the footer with content',
		'before_widget' => '<li>',
		'after_widget' => '</li>',
		'before_title' => '<span style="display:none;">',
		'after_title' => '</span>'
	));
}
add_action('widgets_init', 'ucirvine_widgets_init');

/**
 * Automatically generate hierarchical nav
 * based on specified structure name
 * 
 * @param string $menu Default is pages, which will build nav of site pages, else specified menu name will be used
 */
function ucirvine_get_main_nav($menu = 'pages') {
	if($menu == 'pages') {
		$items = get_pages();
	} else {
                $theme_locations = get_nav_menu_locations();
                $theme_location = wp_get_nav_menu_object($theme_locations[UCI_MENU_MAINNAV]);
                
		$items = wp_get_nav_menu_items($theme_location->term_id, array('sort_column' => 'menu_order'));
	}
	
	$struct = array();
	if(!empty($items)) {
		$i=0;
		foreach($items as $num => $item) {
			$new_item = new stdClass();
			$new_item->url = ($menu == 'pages') ? $item->guid : $item->url;
			$new_item->title = ($menu == 'pages') ? $item->post_title : $item->title;
			
			if($item->menu_item_parent <= 0) {
				$i++;
				
				$struct[$i]['main'] = $new_item;
			} else {
				$struct[$i]['subs'][] = $new_item;
			}
		}
		
		include_once 'templates/uci-nav-list.php';
	} else {
		echo '<hr class="region-divider" />';
	}
}

function ucirvine_get_home_posts($use_static = false) {
	if($use_static == false) {
		return get_posts(array(
			'orderby' 	  => 'post_date',
			'order'  	  => 'DESC',
			'post_status' => 'publish',
			'post_type'   => 'post',
			'numberposts' => 4,
			'tax_query' => array(
				array(
					'taxonomy' => 'post_format',
					'field' => 'slug',
					'terms' => array('post-format-aside', 'post-format-image', 'post-format-link', 'post-format-quote', 'post-format-status'),
					'operator' => 'NOT IN'
				)
			)
		));
	} else {
		return false;
	}
}

function ucirvine_scripts_styles() {
	global $wp_styles;
	
	/*
	 * Adds JavaScript to pages with the comment form to support
	* sites with threaded comments (when in use).
	*/
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
}
add_action('wp_enqueue_scripts', 'ucirvine_scripts_styles');

function ucirvine_excerpt_more($more) {
	return '[&#133;]';
}
add_filter('excerpt_more', 'ucirvine_excerpt_more');

function ucirvine_content_nav($html_id) {
	global $wp_query;
	
	$html_id = esc_attr($html_id);
	
	if($wp_query->max_num_pages > 1) {
		?>
		<nav id="<?php echo $html_id; ?>" class="navigation" role="navigation">
			<h3 class="assistive-text">Post navigation</h3>
			<div class="nav-previous alignleft"><?php next_posts_link( '<span class="meta-nav">&larr;</span> Older posts' ); ?></div>
			<div class="nav-next alignright"><?php previous_posts_link( 'Newer posts <span class="meta-nav">&rarr;</span>' ); ?></div>
		</nav><!-- #<?php echo $html_id; ?> .navigation -->
		<?php
	}
}

function ucirvine_wordmark_hack() {
	$wordmark = get_theme_mod(UCI_THEME_SETTING_WORDMARK);
	
	if($wordmark != "") {
		echo 'background-image:url(' . $wordmark . ');';
	}
}

/**
 * UCI Template Customization registration
 */
function ucirvine_customize_register($wp_customize) {
	$wp_customize->add_setting(UCI_THEME_SETTING_WORDMARK/*, array('transport' => 'postMessage')*/);
        
        $wp_customize->add_setting(UCI_THEME_SETTING_SEARCH, array('default' => UCI_DEFAULT_SEARCH_GSA));
	
	$wp_customize->add_control(new Customize_Wordmark_Control($wp_customize, UCI_THEME_SETTING_WORDMARK, array()));
}
add_action('customize_register', 'ucirvine_customize_register', 10, 1);
?>