<?php wp_enqueue_style('uci-contentpage1'); ?>
<?php wp_enqueue_style('uci-contentpage3'); ?>

<?php get_header(); ?>

<?php while (have_posts()): the_post(); ?>

<h1><?php the_title(); ?></h1>
<?php //if(is_active_sidebar('contentpage1')): ?>
<!-- <nav class="nav-local bucket">
      <ul><?php //dynamic_sidebar('contentpage1'); ?></ul>
</nav> -->
<?php //endif; ?>

<div class="content" style="<?php //echo (!is_active_sidebar('contentpage1')) ? 'width:99%;' : ''; ?>">
	<?php if(!get_post_format()): ?>
	<?php get_template_part('content', ''); ?>
	<?php else: ?>
	<?php get_template_part('content', get_post_format()); ?>
	<?php endif; ?>
	
	<?php get_sidebar(); ?>
	
	<?php if(comments_open()): ?>
	<div><?php comments_template('', true); ?></div>
	<?php endif; ?>
</div>

<?php endwhile; ?>

<?php get_footer(); ?>