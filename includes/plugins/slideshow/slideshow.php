<?php
wp_register_script('ucirvine-slideshow', get_bloginfo('stylesheet_directory') . '/includes/plugins/slideshow/js/slideshow.js', array('jquery'));
wp_register_style('ucirvine-sscss', get_bloginfo('stylesheet_directory') . '/includes/plugins/slideshow/style.css');

class Slideshow_Handler {
	const META_KEY = 'slideshow_image';
	const METABOX_TITLE = 'Slideshow Images';
	
	public function init() {
		add_action('admin_init', array($this, 'admin'));
		
		add_action('wp_ajax_ss_get_attachments', array($this, 'getAttachments'));
		
		add_action('wp_ajax_ss_assign_attachment', array($this, 'assignAttachment'));
		
		add_action('wp_ajax_ss_assign_gallery', array($this, 'assignGallery'));
		
		add_action('wp_ajax_ss_get_images_html', array($this, 'getImagesHTML'));
		
		add_action('wp_ajax_ss_remove_attachment', array($this, 'removeAttachment'));
		
		add_action('add_meta_boxes', array($this, 'metabox'), 10, 2);
		
		add_action('save_post', array($this, 'save'));
	}
	
	public function admin() {
		wp_enqueue_script('ucirvine-slideshow');
		wp_enqueue_style('ucirvine-sscss');
	}
	
	public function removeAttachment() {
		$post_id = $_POST['post_id'];
		$attachment_id = $_POST['attachment_id'];
		
		$update = wp_update_post(array(
			'ID' => $attachment_id,
			'post_parent' => 0
		));
		
		echo json_encode($update);
		die();
	}
	
	public function assignGallery() {
		$post_id = $_POST['post_id'];
		$attachment_ids = $_POST['attachment_ids'];
		
		foreach($attachment_ids as $id) {
			$update = wp_update_post(array(
				'ID' => $id,
				'post_parent' => $post_id
			));
			
			if($update == 0) {
				echo json_encode(0);
				die();
			}
		}
		
		echo json_encode(1);
		die();
	}
	
	public function getImagesHTML() {
		$post_id = $_GET['post_id'];
		
		$attachments = get_posts(array(
			'post_type' => 'attachment',
			'posts_per_page' => -1,
			'post_parent' => $post_id,
			'exclude' => get_post_thumbnail_id($post_id)
		));
		
		if(!empty($attachments)) {
			echo '<div class="clearfix">';
			foreach($attachments as $attachment) {
				$src = wp_get_attachment_image_src($attachment->ID);
				
				echo '<div data-id="' . $attachment->ID . '" class="ss-attachment-item">';
				echo '<img alt="" src="'. $src[0] . '" />';
				echo '<div><a href="#" class="ss-attachment-item-remove" data-id="' . $attachment->ID . '">Remove</a></div>';
				echo '</div>';
			}
			echo '</div>';
		}
		
		die();
	}
	
	public function assignAttachment() {
		$post_id = $_POST['post_id'];
		$attachment_id = $_POST['attachment_id'];
		
		$update = wp_update_post(array(
			'ID' => $attachment_id,
			'post_parent' => $post_id
		));
		
		echo json_encode($update);
		die();
	}
	
	public function getAttachments() {
		$post_id = $_GET['post_id'];
		
		$attachments = get_posts(array(
			'post_type' => 'attachment',
			'posts_per_page' => -1,
			'post_parent' => $post_id,
			'exclude' => get_post_thumbnail_id($post_id)
		));
		
		echo json_encode($attachments);
		die();
	}
	
	public function metabox($post_type, $post) {
		if(post_type_supports($post_type, self::META_KEY)) {
			add_meta_box(self::META_KEY, self::METABOX_TITLE, array($this, 'display'), $post_type, 'normal', 'high');
		} else {
			echo "Nope";
		}
	}
	
	public function save($post_id) {
		if(wp_is_post_autosave($post_id) || wp_is_post_revision($post_id)) {
			return $post_id;
		}
	}
	
	public function display($post) {
		?>
		<div class="uploader">
			<input type="hidden" id="ss_ids" value="" />
			<div id="assigned-attachments"></div>
			<a href="#" id="btn_browse_files" class="button-secondary">Browse Files</a>
			<p>The slide show will appear as the first content area on your page, before any content added in the main content area.</p>
		</div>
		<?php
	}

	public static function getSlideshow($post_id) {
		$images = get_children(array(
			'post_parent' => $post_id,
			'post_type' => 'attachment',
			'numberposts' => -1,
			'post_status' => 'open'
		));
		
		if(!empty($images)) {
			include_once 'plugins/slideshow/templates/slideshow.php';
		}
	}
}

add_action('init', array(new Slideshow_Handler(), 'init'));
?>