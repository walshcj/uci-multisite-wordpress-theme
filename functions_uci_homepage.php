<?php
@define('UCI_HOMEPAGE_ID', 'page_ucihome');
@define('UCI_HOMEPAGE_SLUG', 'homepage');
@define('UCI_THEME_SETTING_HOMEPAGE', 'homepage_page');
@define('UCI_THEME_SETTING_LAYOUT', 'selected_homepage');

add_action('init', 'ucihome_init');
add_action('admin_init', 'ucihome_admin_init');
add_action('save_post', 'ucihome_save');

function ucihome_admin_init() {
	
}

function ucihome_save() {
	global $wpdb;
	global $post;
	
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		return $post->ID;
	}
}

function ucihome_init() {
	register_post_type(UCI_HOMEPAGE_ID, array(
		'labels' => array(
			'name' => _x('UCI Homepages', 'page type general name'),
			'singular_name' => _x('UCI Homepage', 'page type singular name'),
			'add_new' => _x('Add new', 'homepage'),
			'add_new_item' => __('Add new homepage item'),
			'edit_item' => __('Edit homepage'),
			'new_item' => __('New homepage'),
			'view_item' => __('View homepage'),
			'search_items' => __('Search homepages'),
			'not_found' => __('Nothing found'),
			'not_found_in_trash' => __('Nothing found'),
			'parent_item_colon' => ''
		),
		'public' => true,
		'publicly_queryable' => false,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => '',
		'rewrite' => array('slug' => UCI_HOMEPAGE_SLUG, 'with_front' => false),
		'capability-type' => 'page',
		'hierarchical' => false,
		'menu_position' => null,
		'has_archive' => false,
		'supports' => array(
			'title',
			'editor',
			'excerpt',
			Slideshow_Handler::META_KEY
		)
	));
}

function ucihome_pages_select_map($pages) {
	$data[] = '- none -';
	
	foreach($pages as $page) {
		$data[$page->ID] = $page->post_title;
	}
	
	return $data;
}

function ucirvine_has_homepage() {
	$mod = intval(get_theme_mod(UCI_THEME_SETTING_HOMEPAGE));
	
	if($mod != 0) {
		return true;
	}
	
	return false;
}

function ucirvine_get_homepage($post_id) {
	$homepage = get_post($post_id);
	
	include_once 'templates/homepage.php';
}
?>