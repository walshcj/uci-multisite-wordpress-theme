<?php wp_enqueue_style('uci-listpage2'); ?>

<?php get_header(); ?>

<?php if(have_posts()): the_post(); ?>

<h1><?php printf( 'Author Archives: %s', '<span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( "ID" ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>' ); ?></h1>

<?php rewind_posts(); ?>

<?php if(get_the_author('description')): ?>
<div class="intro">
	<?php echo get_avatar(get_the_author_meta('user_email')); ?>
	<h2><?php printf( 'About %s', get_the_author() ); ?></h2>
	<p><?php the_author_meta( 'description' ); ?></p>
</div>
<?php endif; ?>

<?php if(have_posts()): ?>
<div class="buckets">
	<?php while(have_posts()): the_post(); ?>
	<?php get_template_part('content', 'bucket'); ?>
	<?php endwhile; ?>
	
	<?php ucirvine_content_nav('nav-below'); ?>
</div>
<?php else: ?>
<?php get_template_part('content', 'none'); ?>
<?php endif; ?>

<?php endif; ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>