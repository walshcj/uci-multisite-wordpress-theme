jQuery(document).ready(function($) {
	$.when(ssGetImagesHTML($('#post_ID').val())).done(function(a) {
		$('#assigned-attachments').html(a);
	});
	
	var doc = {
		ready: function() {
			if($('#btn_browse_files').length > 0) {
				slider.init();
			}
		}
	},
	
	slider = {
		media_send_attachment:null,
		media_close_window:null,
		media_send_gallery:null,
		init:function() {
			$('#btn_browse_files').click(slider.browse_clicked);
			//console.log(wp.media.gallery.attachments);
		},
		
		browse_clicked: function(event) {
			event.preventDefault();
			
			slider.media_send_attachment = wp.media.editor.send.attachment;
			slider.media_send_gallery = wp.media.gallery.shortcode;
			slider.media_close_window = wp.media.editor.remove;
			
			wp.media.editor.send.attachment = slider.media_accept;
			wp.media.gallery.shortcode = slider.gallery_accept;
			wp.media.editor.remove = slider.media_close;
						
			wp.media.editor.open();
		},
		
		gallery_accept: function(attachments) {
			//console.log(attachments.toJSON());
			
			var selections = attachments.toJSON();
			var id_collection = new Array();
			$(selections).each(function(i, v) {
				id_collection.push(v.id);
			});
			
			$.when(ssAssignGallery(id_collection, $('#post_ID').val())).done(function(a) {
				$.when(ssGetImagesHTML($('#post_ID').val())).done(function(b) {
					$('#assigned-attachments').html(b);
				});
			});
		},
		
		media_accept: function(props, attachment) {
			$.when(ssAssignAttachment(attachment.id, $('#post_ID').val())).done(function(a) {
				$.when(ssGetImagesHTML($('#post_ID').val())).done(function(b) {
					$('#assigned-attachments').html(b);
				});
			});
		},
		
		media_close: function(id) {
			wp.media.editor.send.attachment = slider.media_send_attachment;
			wp.media.gallery.shortcode = slider.media_send_gallery;
			wp.media.editor.remove = slider.media_close_window;
			
			slider.media_send_attachment = null;
			slider.media_send_gallery = null;
			slider.media_close_window = null;
			
			wp.media.editor.remove(id);
		}
	};
	
	$(document).on('click', '.ss-attachment-item-remove', function(e) {
		e.preventDefault();
		
		var attachment_id = $(e.currentTarget).attr('data-id');
		
		$.when(ssRemoveAttachment(attachment_id, $('#post_ID').val())).done(function(a) {
			$(e.currentTarget).parent().parent('.ss-attachment-item').fadeOut('fast', function() {
				$(this).remove();
			});
		});
	});
	
	$(document).ready(doc.ready);
});

function ssRemoveAttachment(attachment_id, post_id) {
	return jQuery.post(ajaxurl, {
		action:'ss_remove_attachment',
		'attachment_id':attachment_id,
		'post_id':post_id
	}, null, 'json');
}

function ssAssignAttachment(attachment_id, post_id) {
	return jQuery.post(ajaxurl, {
		action:'ss_assign_attachment',
		'attachment_id':attachment_id,
		'post_id':post_id
	}, null, 'json');
}

function ssGetImagesHTML(post_id) {
	return jQuery.get(ajaxurl, {
		action:'ss_get_images_html',
		'post_id':post_id
	}, null, 'html');
}

function ssGetAttachments(post_id) {
	return jQuery.get(ajaxurl, {
		'action':'ss_get_attachments',
		'post_id':post_id
	}, null, 'json');
}

function ssAssignGallery(attachment_ids, post_id) {
	return jQuery.post(ajaxurl, {
		action:'ss_assign_gallery',
		'post_id':post_id,
		'attachment_ids':attachment_ids
	}, null, 'json');
}