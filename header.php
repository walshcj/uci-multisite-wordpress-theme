<?php get_template_part('uci', 'header'); ?>

<header>
  <div class="content-container clearfix">
    <nav>
      <?php wp_nav_menu(array(
      	'theme_location' => UCI_MENU_AUDIENCE,
      	'items_wrap' => '<ul>%3$s</ul>',
      	'container' => ''
      )); ?>
    </nav>
    
    <a class="wordmark" href="<?php bloginfo('siteurl'); ?>" style="<?php ucirvine_wordmark_hack(); ?>"><?php bloginfo('name'); ?></a>
    
    <?php if(get_theme_mod(UCI_THEME_SETTING_SEARCH) == UCI_DEFAULT_SEARCH_GSA): ?>
    <form action="http://search1.oit.uci.edu/search" method="get">
      <label for="search-text" class="visuallyhidden">Search</label>
      <input type="text" name="q" title="Enter search term" size="20" id="search-text"/>
      <input type="hidden" name="site" value="uci_full"/>
      <input type="hidden" name="client" value="uci_full"/>
      <input type="hidden" name="proxystylesheet" value="uci_full"/>
      <input type="hidden" name="numgm" value="10"/>
      <input type="hidden" name="ie" value="UTF-8"/>
      <input type="submit" value="search" class="visuallyhidden"/>
    </form>
    <?php else: ?>
    <form role="search" method="get" id="searchform" action="<?php home_url('/'); ?>">
    	<label for="search-text" class="visuallyhidden">Search</label>
    	<input type="text" name="s" title="Enter search term" size="20" id="search-text"/>
    	<input type="submit" value="search" class="visuallyhidden"/>
    </form>
    <?php endif; ?>
  </div>
</header>

<?php get_template_part('uci', 'nav'); ?>

<div role="main"> <!-- begin role="main" -->
	<div class="content-container clearfix"> <!-- begin class="content-container" -->
		<?php if(!is_home() || !is_front_page()): ?>
		<nav class="breadcrumbs"></nav>
		<?php endif; ?>
		
