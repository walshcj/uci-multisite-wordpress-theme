
<div class="text">
	<div><?php the_content( 'Continue reading <span class="meta-nav">&rarr;</span>' ); ?></div>

	<ul>
        <li>by <?php the_author_posts_link(); ?></li>
        <li><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(sprintf('Permalink to %s', the_title_attribute('echo=0'))); ?>" rel="bookmark"><?php echo get_the_date(); ?></a></li>
        
        <?php if(comments_open()): ?>
        <li><?php comments_popup_link( '<span class="leave-reply">' . 'Leave a reply'. '</span>', '1 Reply', '% Replies' ); ?></li>
        <?php endif; ?>
        
        <?php edit_post_link( 'Edit', '<li>', '</li>' ); ?>
        
	</ul>
</div>
