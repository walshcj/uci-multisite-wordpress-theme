<?php get_header(); ?>

<h1>
	<?php
		if ( is_day() ) :
			printf( 'Daily Archives: %s', '<span>' . get_the_date() . '</span>' );
		elseif ( is_month() ) :
			printf( 'Monthly Archives: %s', '<span>' . get_the_date( 'F Y' ) . '</span>' );
		elseif ( is_year() ) :
			printf( 'Yearly Archives: %s', '<span>' . get_the_date( 'Y' ) . '</span>' );
		else :
			'Archives';
		endif;
	?>
</h1>

<div class="content list">
	<div class="text">
		<?php if(have_posts()): ?>

		<?php while (have_posts()): the_post(); ?>
		<h2><?php the_title(); ?></h2>
		
		<?php if(has_post_thumbnail()): ?>
		<div class="main-visual">
			<?php the_post_thumbnail(); ?>
		</div>
		<?php endif; ?>
		
		<div>
			<?php the_content(); ?>
		</div>
	
		<ul>
			<li>by <?php the_author_posts_link(); ?></li>
			<li><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(sprintf('Permalink to %s', the_title_attribute('echo=0'))); ?>" rel="bookmark"><?php echo get_the_date(); ?> </a></li>
			<?php if(comments_open()): ?>
			<li><?php comments_popup_link( '<span class="leave-reply">' . 'Leave a reply'. '</span>', '1 Reply', '% Replies' ); ?></li>
			<?php endif; ?>
			
			<?php edit_post_link( 'Edit', '<li>', '</li>' ); ?>
		</ul>
			
		<?php endwhile; ?>
	
		<?php ucirvine_content_nav('nav-below'); ?>

		<?php else: ?>
		<?php get_template_part('content', 'none'); ?>
		<?php endif; ?>
	</div>
	
	<?php get_sidebar(); ?>
	
</div>

<?php get_footer(); ?>