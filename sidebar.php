<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
	<div class="sidebar <?php echo (get_theme_mod('selected_homepage') == UCI_DEFAULT_UCI_HOMEPAGE && (is_archive() || is_search())) ? '' : 'bucket'; ?>" role="complementary">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</div><!-- #secondary -->
<?php endif; ?>