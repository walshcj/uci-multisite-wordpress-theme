jQuery(function($) {
	wp.customize('ucirvine_wordmark', function(value) {
		value.bind(function(newval) {
			$.when(uciGetWordmark(newval)).done(function(a) {
				console.log(a);
			});
		});
	});
});

function uciGetWordmark(img_url) {
	return jQuery.get(uciajax.ajaxurl, {
		action:'uci_get_wordmark',
		url:img_url
	}, null, 'json');
}