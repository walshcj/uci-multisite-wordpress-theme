<?php 
/**
 * UC Irvine Theme: Bucket template part
 * 
 * Used to list various post formats
 */
?>
<div class="bucket">
	<h2 class="bucket-header"><?php the_title(); ?></h2>
	
	<?php if(has_post_thumbnail()): ?>
	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail(); ?></a>
	<?php endif; ?>
	<p><?php the_excerpt(); ?></p>
	<p><a href="<?php the_permalink(); ?>">Read more</a></p>
</div>