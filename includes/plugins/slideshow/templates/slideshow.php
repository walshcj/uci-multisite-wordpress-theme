<?php
/*print '<pre>';
var_dump($images);
print '</pre>';*/
?>
<div class="main-visual flexslider">
	<ul class="slides">
		<?php foreach($images as $image): ?>
		<li aria-hidden="true">
			<a href="#"><img src="<?php $src = wp_get_attachment_image_src($image->ID, 'full'); echo $src[0]; ?>" alt="<?php echo get_the_title($image->ID); ?>" /></a>
			
			<p class="flex-caption"><?php echo strip_tags($image->post_excerpt); ?>&nbsp;</p>
		</li>
		<?php endforeach; ?>
	</ul>
</div>