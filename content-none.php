

	<div id="post-0" class="post no-results not-found">
		<div class="entry-header">
			<h1 class="entry-title">Nothing found</h1>
		</div>

		<div class="entry-content">
			<p>Apologies, but no results were found. Perhaps searching will help find a related post.</p>
			<?php //get_search_form(); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-0 -->
