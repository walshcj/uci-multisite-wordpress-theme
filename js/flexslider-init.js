/***
 * Flexslider initialization
 * @author Todd McGill, Strategic Communications
 *
 * Includes custom callbacks for accessibiliity and responsive design.
 ***/

$(function() {
    var pSlide = 0; //previously viewed slide
    
    $('.flexslider').flexslider({
        slideshow: true,
        slideshowSpeed: 6000,
        directionNav: false,
        pausePlay: true,
        start: function(slider) {
            ariaShow(-1,slider.animatingTo);
            tallySlides(slider.count);
            if (Modernizr.touch) {
                slider.pause();
            }
        },
        before: function(slider) {
            ariaShow(slider.currentSlide, slider.animatingTo);
        },
        after: function(slider) {
            ariaHide();
        }
    });
    
    function ariaShow(cSlide, nSlide) {
        pSlide = cSlide + 1;
        var targetSlide = '.flexslider .slides li:nth-child(' + (nSlide + 1) + ')';
        $(targetSlide).attr('aria-hidden','false');
    }
    function ariaHide() {
        var targetSlide = '.flexslider .slides li:nth-child(' + pSlide + ')';
        $(targetSlide).attr('aria-hidden','true');
    }
    function tallySlides(count) {
        $('.flex-control-nav').children('li').each(function() {
            var navLink = $(this).find('a');
            navLink.text(navLink.text() + '/' + count);
        });
    }
});
