<?php

?>
<h1 class="visuallyhidden"><?php echo get_the_title($homepage->ID); ?></h1>

<?php Slideshow_Handler::getSlideshow($homepage->ID); ?>

<?php echo $homepage->post_content; ?>