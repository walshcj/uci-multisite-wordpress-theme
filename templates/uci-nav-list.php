<nav id="nav-main">
	<ul class="content-container clearfix">
		<?php foreach($struct as $item): ?>
		
		<li class="news-topic-item">
		
			<a class="nav-main-item" href="<?php echo $item['main']->url; ?>"><?php echo $item['main']->title; ?></a>
			
			<?php if(!empty($item['subs'])): ?>
			
			<ul class="nav-main-menu">
				<?php foreach ($item['subs'] as $sub): ?>
				<li>
					<a class="nav-main-item" href="<?php echo $sub->url; ?>"><?php echo $sub->title; ?></a>
				</li>
				<?php endforeach; ?>
			</ul>
			
			<?php endif; ?>
			
		</li>
		
		<?php endforeach; ?>
	</ul>
</nav>