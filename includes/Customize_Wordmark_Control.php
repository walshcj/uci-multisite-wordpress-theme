<?php
require_once ABSPATH . WPINC . '/class-wp-customize-control.php';

class Customize_Wordmark_Control extends WP_Customize_Image_Control {
	public $label = 'Wordmark';
	public $priority = 30;
	public $description = 'Use this setting to upload your own department or instituion wordmark.';
	public $section = 'title_tagline';
	public $extensions = array('png');
	
	public function __construct($manager, $id, $args) {
		parent::__construct($manager, $id, $args);
		
		//$this->add_tab('library', 'Library', array($this, 'tab_library'));
		add_action('wp_ajax_uci_get_wordmark', array($this, 'getWordmark'));
	}
	
	public function tab_uploaded() {
		$my_context_uploads = get_posts(array(
			'post_type' => 'attachment',
			'orderby' => 'post_date',
			'nopaging' => true
		));
		
		if(empty($my_context_uploads)) {
			return;
		}
		
		foreach((array) $my_context_uploads as $my_context_upload) {
			$path_parts = pathinfo(parse_url(esc_url_raw($my_context_upload->guid), PHP_URL_PATH));
			
			if(in_array($path_parts['extension'], $this->extensions)) {
				$this->print_tab_image(esc_url_raw($my_context_upload->guid));
			}
		}
	}
	
	public function getWordmark() {
		$url = $_GET['url'];
		
		global $wpdb;
		
		$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM " . $wpdb->posts . " WHERE guid = '%s'", $url));
		
		echo json_encode($attachment);
		die();
	}
}
?>