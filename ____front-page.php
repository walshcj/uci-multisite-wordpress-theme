<?php get_header(); ?>

<h1><?php bloginfo('name'); ?></h1>

<div class="intro">
	<p><?php bloginfo('description'); ?></p>
</div>

<?php if(have_posts()): ?>

	<?php while(have_posts()): the_post(); ?>
	<div class="">
		<?php if(!get_post_format()): ?>
		<?php get_template_part('content', ''); ?>
		<?php else: ?>
		<?php get_template_part('content', get_post_format()); ?>
		<?php endif; ?>
	</div>
	<?php endwhile; ?>

<?php endif; ?>

<?php get_sidebar(); ?>

<?php get_footer(); ?>