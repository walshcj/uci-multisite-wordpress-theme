<?php 
if(get_theme_mod('selected_homepage') == UCI_DEFAULT_UCI_HOMEPAGE) {
	wp_enqueue_style('uci-listpage2');
} else if(get_theme_mod('selected_homepage') == UCI_DEFAULT_WP_HOMEPAGE) {
	wp_enqueue_style('uci-contentpage1');
} else {}
?>

<?php get_header(); ?>

<?php if(get_theme_mod('selected_homepage') == UCI_DEFAULT_UCI_HOMEPAGE): ?>

	<h1>Search</h1>
	
	<div class="intro"><p><?php printf( 'Results for: %s', '<span>' . get_search_query() . '</span>' ); ?></p></div>
	
	<?php if(have_posts()): ?>
	<div class="buckets">
		<?php while (have_posts()): the_post(); ?>
		<?php get_template_part('content', 'bucket'); ?>
		<?php endwhile; ?>
		
		<?php ucirvine_content_nav('nav-below'); ?>
	</div>
	<?php else: ?>
	<?php get_template_part('content', 'none'); ?>
	<?php endif; ?>
	
	<?php get_sidebar(); ?>

<?php elseif(get_theme_mod('selected_homepage') == UCI_DEFAULT_WP_HOMEPAGE): ?>

<h1>Search</h1>
<h4><?php printf( 'Results for: %s', '<span>' . get_search_query() . '</span>' ); ?></h4>

<div class="content list">
	<div class="text">
		<?php if(have_posts()): ?>
		
			<?php while (have_posts()): the_post(); ?>
			<h2><?php the_title(); ?></h2>
			
			<?php if(has_post_thumbnail()): ?>
			<div class="main-visual">
				<?php the_post_thumbnail(); ?>
			</div>
			<?php endif; ?>
			
			<div>
				<?php the_content(); ?>
			</div>
		
			<ul class="uci-post-menu clearfix">
				<li>by <?php the_author_posts_link(); ?></li>
				<li><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(sprintf('Permalink to %s', the_title_attribute('echo=0'))); ?>" rel="bookmark"><?php echo get_the_date(); ?> </a></li>
				<?php if(comments_open()): ?>
				<li><?php comments_popup_link( '<span class="leave-reply">' . 'Leave a reply'. '</span>', '1 Reply', '% Replies' ); ?></li>
				<?php endif; ?>
				
				<?php edit_post_link( 'Edit', '<li>', '</li>' ); ?>
			</ul>
			
			<?php endwhile; ?>
			
			<?php ucirvine_content_nav('nav-below'); ?>
		
		<?php else: ?>
		<?php get_template_part('content', 'none'); ?>
		<?php endif; ?>
	</div>
	
	<?php get_sidebar(); ?>
</div>

<?php else: ?>

<?php endif; ?>

<?php get_footer(); ?>