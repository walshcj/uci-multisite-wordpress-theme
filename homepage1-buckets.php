<?php $homepage_posts = ucirvine_get_home_posts(); ?>

<?php if(!empty($homepage_posts)): ?>
<div class="buckets clearfix">
	<?php foreach($homepage_posts as $post): setup_postdata($post); ?>
	<div class="bucket">
		<h2 class="bucket-header"><?php the_title(); ?></h2>
		<a href="<?php the_permalink(); ?>"><?php if(has_post_thumbnail()): the_post_thumbnail(); endif; ?></a>
		<p><?php the_excerpt(); ?></p>
	</div>
	<?php endforeach; ?>
</div>

<?php else: ?>

<div class="buckets clearfix">
      <div class="bucket">
        <h2 class="bucket-header">header</h2>
        <img src="http://communications.uci.edu/img/graphic-identity/placeholder.png" alt="">
        <p><a href="#">bucket content goes here</a></p>
        <p><a href="#">some more content here too</a></p>
      </div>
      <div class="bucket">
        <h2 class="bucket-header">header</h2>
        <img src="http://communications.uci.edu/img/graphic-identity/placeholder.png" alt="">
        <p><a href="#">bucket content goes here</a></p>
        <p><a href="#">some more content here too</a></p>
      </div>
      <div class="bucket">
        <h2 class="bucket-header">header</h2>
        <img src="http://communications.uci.edu/img/graphic-identity/placeholder.png" alt="">
        <p><a href="#">bucket content goes here</a></p>
        <p><a href="#">some more content here too</a></p>
      </div>
      <div class="bucket">
        <h2 class="bucket-header">header</h2>
        <img src="http://communications.uci.edu/img/graphic-identity/placeholder.png" alt="">
        <p><a href="#">bucket content goes here</a></p>
        <p><a href="#">some more content here too</a></p>
      </div>
    </div>

<?php endif; ?>